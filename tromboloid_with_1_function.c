//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
    float h,d,b,volume;
    
    printf("Enter the value of h\n");
    scanf("%f",&h);
    printf("Enter the value of d\n");
    scanf("%f",&d);
    printf("Enter the value of b\n");
    scanf("%f",&b);
    volume=((h*d*b)/3)+((d/b)/3);
    printf("The volume of the Tromboloid with h=%f,d=%f and b=%f is %f",h,d,b,volume);
    return 0;
}